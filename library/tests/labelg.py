
import subprocess
import os

def batchGetCanonicalLabel(labels: list, knownLabels: dict = {}) -> dict:
    #print(os.path.dirname(__file__) + '/../../../resources/')
    f = open('input.txt', 'w+')
    input = []

    # add labels without known canonical labels to input.txt
    for inputs in labels:
        if (inputs not in knownLabels):
            f.write(inputs+ '\n')
            input.append(inputs)
    f.close()
    # run labelg for the unknown labels
    cmd = [os.path.dirname(__file__) + '/labelg', 'input.txt', 'output.txt']
    p = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    p.wait()

    # add newly found labels: canonical label key values pairs to knownLabels
    f = open('output.txt', 'r')
    
    for lines in f:
        if lines.strip() in knownLabels:
            knownLabels.update({lines.strip():knownLabels.get(lines.strip())+1})
        else:
            knownLabels.update({lines.strip():1})
    
    f.close()

    os.remove('input.txt')
    os.remove('output.txt')

    return knownLabels

