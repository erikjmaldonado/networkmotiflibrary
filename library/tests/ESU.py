
import Graph

def EnumerateSubgraphs(oGraph: Graph, iSubgraphSize: int):
    lResult = list()
    # get vertex
    for iVertex in oGraph.getGraph():
        # subgraph formation
        sFormation = set([iVertex])
        # for this vertex get greater neighbors while not size of subgraph
        ExtendSubgraph(oGraph, sFormation, oGraph.GreaterSet(
            sFormation, iVertex, iVertex), iVertex, iSubgraphSize-1, lResult)
    return lResult


def ExtendSubgraph(oGraph: Graph, sFormation: set, sNeighbors: set, iVertex: int, iLevel: int, lResult: list):
    # level 0 means we have formed the intended size of subgraph
    if iLevel == 0:
        lResult.append(oGraph.getG6Label(sFormation))
        
    else:
        if len(sNeighbors) != 0:
            # exclude everything already in subgraph formation
            sExclusions = set(sFormation)
            dDependices = dict()
            # try to extend each neighbor
            for iNeighbor in sNeighbors.copy():
                # remove arbitrary vertex from neighbor
                if iNeighbor in sNeighbors:
                    sNeighbors.remove(iNeighbor)
                    # add it to the subgraph formation
                    sFormation.add(iNeighbor)
                    # add it to exclusions
                    sExclusions.add(iNeighbor)
                    # update neighbors to include new neighbors only if not legnth
                    if iLevel-1 != 0:
                        sNewNeighbors = oGraph.GreaterSet(
                            sExclusions, iNeighbor, iVertex)
                        dDependices.update({iNeighbor: sNewNeighbors})
                        sNeighbors.update(sNewNeighbors)
                    # continue extending subgraph
                    ExtendSubgraph(oGraph, sFormation, sNeighbors.copy(), iVertex, 
                        iLevel-1, lResult)
                    # remove from formation
                    sFormation.remove(iNeighbor)
                    # remove neihbors dependancy
                    if iNeighbor in dDependices:
                        for iDependency in dDependices.get(iNeighbor):
                            sNeighbors.remove(iDependency)
