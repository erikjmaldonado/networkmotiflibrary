import random
class Graph:
    '''Implementation of Graph Data structure to manage Graphs'''
    graph = {}
    F = None
    oDegreeSequenceVector = []
    
    def __init__(self):
        self.graph = {}
        self.F = None

    def fileToGraph(self, fileName):
        self.F = open(fileName, "r")
        F1 = self.F.readlines()
        curr = 0;
        
        for edge in F1:
            curr = edge.split("\t")
            if len(curr) < 2:
                curr = edge.split(" ")
            curr[1] = curr[1].strip('\n')
            if int(curr[0]) in self.graph:
                self.graph[int(curr[0])].add(int(curr[1]))
            else:
                self.graph[int(curr[0])] = set([])
                self.graph[int(curr[0])].add(int(curr[1]))

            if int(curr[1]) in self.graph:
                if int(curr[0]) in self.graph[int(curr[1])]:
                    break
                else:
                    self.graph[int(curr[1])].add(int(curr[0]))
            else:
                self.graph[int(curr[1])] = set([])
                self.graph[int(curr[1])].add(int(curr[0]))
                
                
    def printGraph(self):
        for x in self.graph:
            print("edges for: ", x)
            for i in self.graph[x]:
                print(i)
                
    def getGraph(self):
        return self.graph
    
    def GreaterSet(self, sExclusions: set, iVertex: int, iValue: int):
        '''Return the all neighbors from [iVertex] greater than [iValue]'''
        sGreaterSet = set()
        for iNeighbor in self.graph[iVertex]:
            if iNeighbor > iValue and iNeighbor not in sExclusions:
                sGreaterSet.add(iNeighbor)
        return sGreaterSet

    def getDegreeSequenceVector(self):
        dsv = []
        for i,value in self.graph.items():
            dsv.append(len(value))

        return dsv
            
    def generateGraph(self):
            # result
            dNewGraph = dict()
            # randomly s
            self.oDegreeSequenceVector = self.getDegreeSequenceVector()
            random.shuffle(self.oDegreeSequenceVector)
                                                
            # get size
            iGraphSize = len(self.oDegreeSequenceVector) - 1
            # for each vertex's degree
            for iIndex, iVertex in enumerate(self.oDegreeSequenceVector):
                # if set for vertext does not exist
                if iIndex not in dNewGraph:
                    dNewGraph[iIndex] = set()
                # generate a random value for each iteration
                for iIteration in range(0, iVertex):
                    iRandom = random.randint(0, iGraphSize)
                    count = 0;
                    while iRandom == iIndex\
                          or iRandom in dNewGraph[iIndex]:
                        iRandom = random.randint(0, iGraphSize)
                        count += 1
                        if(count > iGraphSize * 2):
                            iRandom = count + 1
                    # add to vertices in new graph
                    dNewGraph[iIndex].add(iRandom)
                    if iRandom not in dNewGraph:
                        dNewGraph[iRandom] = set()
                    dNewGraph[iRandom].add(iIndex)
            oNewGraph = Graph()
            oNewGraph.graph = dNewGraph
            return oNewGraph
                     
        
    
    def getG6Label(self, sub: set) -> str:
        binary = []
        myList = []
        for i in sub:
            myList.append(i);
        for i, value in enumerate(myList):
            for x in range(0, i):
                if myList[x] in self.graph[value]: 
                    binary.append("1")
                else:
                    binary.append("0")
        while len(binary) % 6 != 0:
            binary.append('0')
        binary = "".join(binary)
        
        chars = []
        num_segments = int(len(binary) / 6)
        for i in range(0, num_segments):
            chars.append(binary[i * 6: i * 6 + 6])

        rx = []
        for char in chars:
            binary = int(char, 2)
            binary += 63
            binary = bin(binary)
            binary = binary[2:]
            #binary = binary[len(binary) - 6:]
            rx.append(chr(int(binary, 2)))
        rx = "".join(rx)
        nn = chr(63 + len(sub))
        return nn + rx


