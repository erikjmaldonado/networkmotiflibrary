
import argparse
import sys

import labelg
import ESU
import Graph
import Statistics
from copy import deepcopy
# get input file subgraph siiZScoree and random generations


def UpdateProgress(fProgress: float, sEnd: str = "DONE!") -> None:
    """
    Displays to the screen the progress of a particular task as specified by
    [fProgress].
    """
    iBarWidth = 50
    iBlocks = round(iBarWidth*fProgress)
    fPercent = round(fProgress*100, 1)
    sProgress = "#"*iBlocks + "-"*(iBarWidth-iBlocks)
    # create status string
    if fProgress >= 1:
        sStatus = "\r%\t[{0}] {1}\r\n".format(sProgress, sEnd)
    else:
        sStatus = "\r%\t[{0}] {1}%".format(sProgress, fPercent)
    # write out put then flush
    sys.stdout.write(sStatus)
    sys.stdout.flush()

def ParseArguments():
    parser = argparse.ArgumentParser(
        description="ESU algorithm implementation optimiiZScoreed for PyPy",
        prog="ESU",)
    
    parser.add_argument(
        "-File",
        help="input file that represents a graph",
        required=True)

    parser.add_argument(
        "-SubgraphSize",
        help="size of the subgraph to search for",
        required=True)

    parser.add_argument(
        "-RandomGenerations",
        help="size of the subgraph to search for",
        required=True)

    return parser.parse_args()

if __name__ == "__main__":
    # parse arguments
    lArguments = ParseArguments()
    # create and load graph
    graph = Graph.Graph()
    graph.fileToGraph(lArguments.File)
    # perform esu on graph
    dReal = labelg.batchGetCanonicalLabel(ESU.EnumerateSubgraphs(graph, int(lArguments.SubgraphSize))).copy()
    
    # storage for random generations
    dGenerated = dict()
    
    iGenerations = int(lArguments.RandomGenerations)

    print("Enumerating, generating, and labeling graphs")

    for i in range(0, iGenerations):
        UpdateProgress(i/iGenerations)
        # generate a graph
        nGraph = graph.generateGraph()
        # store in temp dict
        dTemp = labelg.batchGetCanonicalLabel(ESU.EnumerateSubgraphs(nGraph, int(lArguments.SubgraphSize))).copy()
        # reduce dTemp into dGenerated
        for oKey in dTemp:
            if oKey in dGenerated:
                dGenerated.update({oKey:dTemp.get(oKey)+dGenerated.get(oKey)})
            else:
                dGenerated.update({oKey:dTemp.get(oKey)})
    UpdateProgress(1)

    dRealRelative = Statistics.relativeFrequency(dReal)
    dGeneratedRelative = Statistics.relativeFrequency(dGenerated)

    iMeans = Statistics.randomMeanFrequency(dRealRelative , dGeneratedRelative)


    dStandard = Statistics.standardDeviation(dRealRelative, dGeneratedRelative, iMeans)
    iZScore = Statistics.zScore(dRealRelative, iMeans, dStandard)
    iPValue = Statistics.pValue(iZScore)

    all_labels = set()
    for label in dReal:
        all_labels.add(label)

    # print(data_labels)

    print('Label \t RelFrequency \t RandMeanFreq \t Z-Score \t P-Value')
    for label in all_labels:
        if label not in dRealRelative:
            dRealRelative[label] = 0
        if label not in iMeans:
            iMeans[label] = 0
        print(label, '\t', '{0:.4f}'.format(dRealRelative[label] * 100), '%', end=' \t ')
        print('{0:.4f}'.format(iMeans[label] * 100), '%',  '\t', '{0:.4f}'.format(iZScore[label]), '\t', '{0:.4f}'.format(iPValue[label]))
