# NetworkMotifLibrary

Network motif library implemented in python and tested using the PyPy3 interpretor.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

[PyPy3 Interpreter](https://www.pypy.org/download.html)

```
bzip2 -b pypy3.5-v7.0.0-linux64.tar.bz2
```

Packages

```
pip
```


### Installing

A step by step series of examples that tell you how to get a development environment running

Clone the repository

```
git clone https://erikjmaldonado@bitbucket.org/erikjmaldonado/networkmotiflibrary.git
```

To run

```
instructions example
```

Example

```
example instructions & output
```

## Running the tests

How to run automated tests for the program

### Test1

Explain what these tests test and why

```
test example
```

## Built With

* [Python3](https://docs.python.org/3/)
* [PyPy3](http://doc.pypy.org/en/latest/)

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

* **Erik Maldonado**
* **Khuzaima Mushtaq**

## Acknowledgments

* Hat tips
